package com.contestantbots.team;

import com.contestantbots.util.GameStateLogger;
import com.contestantbots.util.NoViableMoveException;
import com.contestantbots.util.RouteNotFoundException;
import com.scottlogic.hackathon.client.Client;
import com.scottlogic.hackathon.game.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SmitherBot extends Bot {

  private final GameStateLogger gameStateLogger;

  private Map<Player, Position> assignedPlayerDestinations;

  private Map<Position, Destination> assignedPlayerRoutes;

  private List<Position> nextPositions;

  private Map<Position, Integer> exploredTime;

  public SmitherBot() {
    super("Smither Bot");
    gameStateLogger = new GameStateLogger(getId());
  }

  /*
   * Run this main as a java application to test and debug your code within your IDE.
   * After each turn, the current state of the game will be printed as an ASCII-art
   * representation in the console.
   * You can study the map before hitting 'Enter' to play the next phase.
   */
  public static void main( String ignored[] ) throws Exception {
    final String[] args = new String[]{
        /*
        Pick the map to play on
        -----------------------
        Each successive map is larger, and has more out-of-bounds positions that must be avoided.
        Make sure you only have ONE line uncommented below.
         */
        "--map",
        //                    "VeryEasy",
        "Easy",
        //                    "Medium",
        //                    "LargeMedium",
        //                    "Hard",
        /*
        Pick your opponent bots to test against
        ---------------------------------------
        Every game needs at least one opponent, and you can pick up to 3 at a time.
        Uncomment the bots you want to face, or specify the same opponent multiple times to face
        multiple
        instances of the same bot.
         */
        "--bot",
        //                    "Default", // Players move in random directions
        "Milestone1",
        // Players just try to stay out of trouble
        //                    "Milestone2", // Some players gather collectables, some attack
        // enemy players, and some attack enemy spawn points
        //                    "Milestone3", // Strategy dynamically updates based on the current
        // state of the game
        /*
        Enable debug mode
        -----------------
        This causes all Bots' 'makeMoves()' methods to be invoked from the main thread,
        and prevents them from being disqualified if they take longer than the usual time limit.
        This allows you to run in your IDE debugger and pause on break points without timing out.

        Comment this line out if you want to check that your bot is running fast enough.
         */
        "--debug",
        // Use this class as the 'main' Bot
        "--className", SmitherBot.class.getName()
    };
    Client.main(args);
  }

  private List<Move> assignRoutes(
      final GameState gameState, final Map<Player, Position> assignedPlayerDestinations,
      final List<Position> nextPositions, List<Destination> routes
  ) {
    return routes.stream().filter(
        route -> !assignedPlayerDestinations.containsKey(route.getPlayer()) &&
                 !assignedPlayerDestinations.containsValue(route.getDestination())).map(route -> {
      Optional<Direction> direction = gameState.getMap().directionsTowards(
          route.getPlayer().getPosition(), route.getDestination()).findFirst();
      if (direction.isPresent() && canMove(
          gameState, nextPositions, route.getPlayer(), direction.get())) {
        assignedPlayerDestinations.put(route.getPlayer(), route.getDestination() );
        return new MoveImpl(route.getPlayer().getId(), direction.get());
      }
      return null;
    }).filter(move -> move != null).collect(Collectors.toList());
  }

  private boolean canMove(
      final GameState gameState, final List<Position> nextPositions, final Player player,
      final Direction direction
  ) {
    Set<Position> outOfBounds = gameState.getOutOfBoundsPositions();
    Position newPosition = gameState.getMap().getNeighbour(player.getPosition(), direction);
    if (!nextPositions.contains(newPosition) && !outOfBounds.contains(newPosition)) {
      nextPositions.add(newPosition);
      return true;
    } else {
      return false;
    }
  }

  private List<Move> doCollect(
      final GameState gameState, final Map<Player, Position> assignedPlayerDestinations,
      final List<Position> nextPositions
  ) {
    Set<Position> collectablePositions = gameState.getCollectables().stream().map(
        collectable -> collectable.getPosition()).collect(Collectors.toSet());
    Set<Player> players = gameState.getPlayers()
        .stream()
        .filter(player -> isMyPlayer(player))
        .collect(Collectors.toSet());
    List<Destination> collectableDestinations = new ArrayList<>();
    List<Move> collectMoves = new ArrayList<>();
    for (Position collectablePosition : collectablePositions) {
      if (assignedPlayerDestinations.values().contains(collectablePosition)) {
        try {
          collectMoves.add(new MoveImpl(assignedPlayerRoutes.get(collectablePosition)
              .getPlayer()
              .getId(), assignedPlayerRoutes.get(collectablePosition).getNextDirection(gameState)));
        } catch (RouteNotFoundException e) {
          assignedPlayerDestinations.remove(collectablePosition);
          assignedPlayerRoutes.remove(collectablePosition);
        } catch (NoViableMoveException e) {
          if (assignedPlayerRoutes.get(collectablePosition).getSequentialMissedMoves() > 3) {
            assignedPlayerDestinations.remove(collectablePosition);
            assignedPlayerRoutes.remove(collectablePosition);
          }
        }
      } else {
        for (Player player : players) {
          int distance = gameState.getMap().distance(player.getPosition(), collectablePosition);
          Destination destination = new Destination(player, collectablePosition, distance);
          collectableDestinations.add(destination);
        }
      }
    }
    collectableDestinations.sort(Destination::compareTo);
    for (Destination destination : collectableDestinations) {
      if (!assignedPlayerDestinations.containsKey(destination.getPlayer()) &&
          !assignedPlayerDestinations.containsValue(destination.getDestination())) {
        Optional<Direction> direction = gameState.getMap().directionsTowards(
            destination.getPlayer().getPosition(), destination.getDestination()).findFirst();
        if (direction.isPresent() && canMove(
            gameState, nextPositions, destination.getPlayer(), direction.get())) {
          collectMoves.add(new MoveImpl(destination.getPlayer().getId(), direction.get()));
          assignedPlayerDestinations.put(destination.getPlayer(), destination.getDestination());
        }
      }
    }
    System.out.println(collectMoves.size() + " players collecting");
    return collectMoves;
  }

  private List<Move> doExplore( final GameState gameState, final List<Position> nextPositions ) {
    List<Move> exploreMoves = new ArrayList<>();
    exploreMoves.addAll(gameState.getPlayers()
        .stream()
        .filter(player -> isMyPlayer(player))
        .filter(player -> !assignedPlayerDestinations.containsKey(player))
        .map(player -> doMove(gameState, nextPositions, player))
        .filter(move -> move != null)
        .collect(Collectors.toList()));
    System.out.println(exploreMoves.size() + " players exploring");
    return exploreMoves;
  }

  private List<Move> doExploreUnseen(
      final GameState gameState, final Map<Player,Position> assignedPlayerDestinations, final List<Position> nextPositions
  ) {
    List<Move> exploreMoves = new ArrayList<>();
    Set<Player> players = gameState.getPlayers()
        .stream()
        .filter(player -> isMyPlayer(player))
        .filter(player -> !assignedPlayerDestinations.containsKey(player))
        .collect(Collectors.toSet());
    Set<Position> toBeExplored = new HashSet<>();
    int targetedPhase = 0;
    while (toBeExplored.size() < players.size() && targetedPhase < gameState.getPhase()) {
      if (exploredTime.values().contains(targetedPhase)) {
        for (Position position : exploredTime.keySet()) {
          if (exploredTime.get(position) == targetedPhase) {
            toBeExplored.add(position);
          }
        }
      }
      targetedPhase++;
    }
    List<Destination> unseenRoutes = generateRoutes(gameState, players, toBeExplored);
    Collections.sort(unseenRoutes);
//    exploreMoves.addAll(
//        assignRoutes(gameState, assignedPlayerDestinations, nextPositions, unseenRoutes));
    exploreMoves.addAll()
    System.out.println(exploreMoves.size() + " players exploring unseen");
    return exploreMoves;
  }

  private Move doMove(
      final GameState gameState, final List<Position> nextPositions, final Player player
  ) {
    List<Direction> directions = new ArrayList<>(Arrays.asList(Direction.values()));
    Direction direction;
    do {
      direction = directions.remove(ThreadLocalRandom.current().nextInt(directions.size()));
    } while (!directions.isEmpty() && !canMove(gameState, nextPositions, player, direction));
    return new MoveImpl(player.getId(), direction);
  }

  private List<Destination> generateRoutes(
      final GameState gameState, Set<Player> players, Set<Position> destinations
  ) {
    List<Destination> routes = new ArrayList<>();
    for (Position destination : destinations) {
      for (Player player : players) {
        int distance = gameState.getMap().distance(player.getPosition(), destination);
        Destination route = new Destination(player, destination, distance);
        routes.add(route);
      }
    }
    return routes;
  }

  private Stream<Position> getSurroundingPositions(
      final GameState gameState, final Position position, final int distance
  ) {
    return IntStream.rangeClosed(-distance, distance).mapToObj(
        x -> IntStream.rangeClosed(-distance, distance)
            .mapToObj(y -> gameState.getMap().createPosition(x, y))).flatMap(Function.identity());
  }

  @Override
  public void initialise( final GameState initialGameState ) {
    assignedPlayerRoutes = new HashMap<>();
    exploredTime = new HashMap<>();
    for (int y = 0 ; y < initialGameState.getMap().getWidth() ; y++) {
      for (int x = 0 ; x < initialGameState.getMap().getHeight() ; x++) {
        exploredTime.put(new Position(x, y), -1);
      }
    }
  }

  private boolean isMyPlayer( final Player player ) {
    return player.getOwner().equals(getId());
  }

  @Override
  public List<Move> makeMoves( final GameState gameState ) {
    updateUnseenLocations(gameState);
    gameStateLogger.process(gameState);
    assignedPlayerDestinations = new HashMap<>();
    nextPositions = new ArrayList<>();
    List<Move> moves = new ArrayList<>();
    moves.addAll(doCollect(gameState, assignedPlayerDestinations, nextPositions));
    moves.addAll(doExploreUnseen(gameState, assignedPlayerDestinations, nextPositions));
    return moves;
  }

  private void updateUnseenLocations( final GameState gameState ) {
    // assume players can 'see' a distance of 5 squares
    int visibleDistance = 4;
    final Set<Position> visiblePositions = gameState.getPlayers()
        .stream()
        .filter(player -> isMyPlayer(player))
        .map(player -> player.getPosition())
        .flatMap(
            playerPosition -> getSurroundingPositions(gameState, playerPosition, visibleDistance))
        .distinct()
        .collect(Collectors.toSet());
    // remove any positions that can be seen
    visiblePositions.stream().forEach(position -> exploredTime.put(position, gameState.getPhase()));
  }

  public class Destination implements Comparable<Destination> {

    private final Player player;

    private final Position destination;

    private final int distance;

    private Route route;

    private int missedMoves = 0;

    public Destination( Player player, Position destination, int distance ) {
      this.player = player;
      this.destination = destination;
      this.distance = distance;
    }

    public boolean calcRoute( GameState gs ) throws RouteNotFoundException {
      Optional<Route> possible = gs.getMap().findRoute(player.getPosition(), destination,
          gs.getOutOfBoundsPositions()
      );
      if (possible.isPresent()) {
        route = possible.get();
        return true;
      } else {
        throw new RouteNotFoundException();
      }
    }

    @Override
    public int compareTo( Destination o ) {
      return distance - o.getDistance();
    }

    public Position getDestination() {
      return destination;
    }

    public int getDistance() {
      return distance;
    }

    Direction getNextDirection( GameState gs ) throws RouteNotFoundException,
        NoViableMoveException {
      if (route == null) {
        calcRoute(gs);
      }
      if (route.getFirstDirection().isPresent() && canMove(gs, nextPositions, player,
          route.getFirstDirection().get()
      )) {
        return route.getFirstDirection().get();
      } else if (calcRoute(gs) && route.getFirstDirection().isPresent() && canMove(gs,
          nextPositions, player, route.getFirstDirection().get()
      )) {
        return route.getFirstDirection().get();
      } else {
        missedMoves++;
        throw new NoViableMoveException();
      }
    }

    public Player getPlayer() {
      return player;
    }

    public int getSequentialMissedMoves() {
      return missedMoves;
    }

  }

  public class MoveImpl implements Move {

    private UUID playerId;

    private Direction direction;

    public MoveImpl( UUID playerId, Direction direction ) {
      this.playerId = playerId;
      this.direction = direction;
    }

    @Override
    public Direction getDirection() {
      return direction;
    }

    @Override
    public UUID getPlayer() {
      return playerId;
    }

  }

}
